﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace ipswBalance
{
    /// <summary>
    /// 用ServiceContract来标记此接口是WCF的服务契约，可以像WebService一样指定一个Namespace，如果不指定，就是默认的http://tempuri.org
    /// </summary>
    [ServiceContract(Namespace = "IPSW.Balance")]
    public interface IData
    {
        /// <summary>
        /// 用OperationContract来标记此方法是操作契约
        /// </summary>
        [OperationContract]
        string GetIPSWDirectory(string sIpswpath);
    }
    public class DataProvider : IData
    {
        static string NETTCPURL = @"net.pipe://localhost/IPSW";
        static AllHDDInfoTree infoall = new AllHDDInfoTree();
        static int iQueryTime = 0;
        public string GetIPSWDirectory(string sIpswpath)
        {
            if(infoall.GetHDDCount()<2)
            {
                return sIpswpath;
            }
            List<string> ipswfolders = infoall.GetBalanceDir(sIpswpath);
            if(ipswfolders.Count == 0)
            {
                Program.logIt("Can not find the other disk dmg");
                return sIpswpath;
            }
           
            int i = (iQueryTime % ipswfolders.Count);
            iQueryTime++;
            return ipswfolders[i];
        }

        public static void StartServer(System.Threading.EventWaitHandle quit)
        {
            Uri tcpAddress = new Uri(NETTCPURL);
            Type serviceType = typeof(ipswBalance.DataProvider);

            infoall.InitDiskDrive();

            using (ServiceHost host = new ServiceHost(serviceType, new Uri[] {tcpAddress }))
            {
                
                //再来一个netTcpBinding 
                //NetTcpBinding netTcpBinding = new NetTcpBinding();
                NetNamedPipeBinding netpipeBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.Transport);
                netpipeBinding.MaxConnections = 128;
                //netTcpBinding.HostNameComparisonMode = HostNameComparisonMode.Exact;
                netpipeBinding.HostNameComparisonMode = HostNameComparisonMode.Exact;
                string address = NETTCPURL;
                host.AddServiceEndpoint(typeof(ipswBalance.IData), netpipeBinding, address);

                //开始服务 
                host.Open();
                Console.WriteLine("Service Running ...");
                quit.WaitOne();
                host.Close();
            }  
        }

        public static string QueryInfo(string sPath)
        {
            Binding tcpBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.Transport);
            EndpointAddress tcpAddr = new EndpointAddress(NETTCPURL);
            var proxy = new ChannelFactory<ipswBalance.IData>(tcpBinding, tcpAddr).CreateChannel();
            string s = proxy.GetIPSWDirectory(sPath);
            ((IChannel)proxy).Close();

            return s;
        }
    } 
}
