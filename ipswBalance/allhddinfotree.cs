﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;

namespace ipswBalance
{
    public class AllHDDInfoTree
    {
        public static string GetAPSTHOMECommon()
        {
            string s = @"C:\";
            string ss = System.Environment.GetEnvironmentVariable("APSTHOME");
            if (!string.IsNullOrEmpty(ss) && System.IO.Directory.Exists(ss))
            {
                s = System.IO.Path.Combine(ss, @"PST\Common\Apple");
            }
            else
            {
                Trace.WriteLine(string.Format("APSTHOME not find. {0}", ss));
            }

            return s;
        }

        public class HDDInfoTree
        {
            public string sModel;
            public string sName;
            public class PartitionInfo
            {
                public string sPartition;
                public List<string> Diskes = new List<string>();
            }
            public List<PartitionInfo> partitions = new List<PartitionInfo>();
        }

        public List<HDDInfoTree> infotrees = new List<HDDInfoTree>();

        public static string ListStringToString(List<string> ss)
        {
            return string.Join(",", ss.ToArray());
        }

        String GetDeviceIdStr(string sDiviceId)
        {
            string s = string.Empty;
            string sPatten = ".*?=\"(.*?)\"";
            Match match = Regex.Match(sDiviceId, sPatten, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                // Finally, we get the Group value and display it.
                s = match.Groups[1].Value;
                Program.logIt(s);
            }

            return s;
        }


        public int GetHDDCount()
        {
            return infotrees.Count;
        }
        /// <summary>
        /// get HDD NAME from logic Disk
        /// </summary>
        /// <param name="s">s is for example is "C:" or "C" or "C:\"</param>
        /// <returns></returns>
        public String GetHDDNameFromLogicDisk(string s)
        {
            string sret = String.Empty;
            if (string.IsNullOrEmpty(s)) return sret;

            if (s.Length > 2) s = s.Substring(0, 2);
            else if (s.Length == 1) s += ":";

            Boolean bFind = false;
            foreach (var hdd in infotrees)
            {
                foreach (var pp in hdd.partitions)
                {
                    foreach (var disk in pp.Diskes)
                    {
                        if (string.Compare(disk, s, true) == 0)
                        {
                            sret = hdd.sName;
                            bFind = true;
                            break;
                        }
                    }
                    if (bFind) break;
                }
                if (bFind) break;
            }

            return sret;
        }


        public List<string> GetBalanceDir(string s)
        {
            List<string> filenames = new List<string>();
            filenames.Add(s);
            string sSysDisk= GetHDDNameFromLogicDisk(s);
            if(string.IsNullOrEmpty(sSysDisk))
            {
                Program.logIt("Can not Find System HDD.");
                filenames.Add(s);
                return filenames;
            }

            Dictionary<String, List<string>> systemDisks = GetDiskLogicalDiskExceptSys(sSysDisk);
            foreach (var hd in systemDisks)
            {
                foreach(var disk in hd.Value)
                {
                    string sNewPath = disk + s.Substring(2);
                    Program.logIt(sNewPath);
                    if(System.IO.File.Exists(sNewPath))
                    {
                        filenames.Add(sNewPath);
                        break;
                    }
                }
            }

            return filenames;
        }
        /// <summary>
        /// except Sys HDD.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Dictionary<String,List<string>> GetDiskLogicalDiskExceptSys(string s)
        {
            Dictionary<String, List<string>> ret = new Dictionary<String, List<string>>();

            foreach (var hdd in infotrees)
            {
                List<string> logdisks = new List<string>();
                ret.Add(hdd.sName, logdisks);
                if (string.IsNullOrEmpty(s) || string.Compare(s, hdd.sName, true) != 0)
                {
                    foreach (var pp in hdd.partitions)
                    {
                        foreach (var disk in pp.Diskes)
                        {
                            logdisks.Add(disk);
                        }
                    }
                }
            }

            return ret;
        }


        public List<string> GetDiskLogicalDisk(string s)
        {
            List<string> logdisks = new List<string>();

            foreach (var hdd in infotrees)
            {
                if (string.IsNullOrEmpty(s) || string.Compare(s, hdd.sName, true) == 0)
                {
                    foreach (var pp in hdd.partitions)
                    {
                        foreach (var disk in pp.Diskes)
                        {
                            logdisks.Add(disk);
                        }
                    }
                }
            }

            return logdisks;
        }


        public List<string> GetAllLogicalDisk()
        {
            List<string> logdisks = new List<string>();

            foreach (var hdd in infotrees)
            {
                foreach (var pp in hdd.partitions)
                {
                    foreach (var disk in pp.Diskes)
                    {
                        logdisks.Add(disk);
                    }
                }
            }

            return logdisks;
        }

        public void InitDiskDrive()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive WHERE MediaType LIKE 'Fixed hard disk%'");
            foreach (ManagementObject share in searcher.Get())
            {
                HDDInfoTree intr = new HDDInfoTree();
                //Trace.WriteLine(share.ToString());
                Trace.WriteLine(share["Name"].ToString());
                intr.sName = share["Name"].ToString();
                Trace.WriteLine(share["Model"].ToString());
                intr.sModel = share["Model"].ToString();
                infotrees.Add(intr);
                foreach (ManagementObject DPObj in share.GetRelated("Win32_DiskPartition"))
                {
                    Trace.WriteLine(DPObj.ToString());
                    HDDInfoTree.PartitionInfo pin = new HDDInfoTree.PartitionInfo();
                    pin.sPartition = GetDeviceIdStr(DPObj.ToString());
                    intr.partitions.Add(pin);
                    foreach (ManagementObject LDObj in DPObj.GetRelated("Win32_LogicalDisk"))
                    {
                        Trace.WriteLine(LDObj.ToString());
                        pin.Diskes.Add(GetDeviceIdStr(LDObj.ToString()));
                    }
                }
            }

        }

    }

}
