﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;

namespace ipswBalance
{
    class Program
    {
        static int _nLabel = 0;
        public static void logIt(string s)
        {
            try
            {
                if(_nLabel == 0)
                {
                    System.Diagnostics.Trace.WriteLine(string.Format("[{1}]:{0}", s, System.AppDomain.CurrentDomain.FriendlyName));
                }
                else
                {
                    System.Diagnostics.Trace.WriteLine(string.Format("[Label_{1}]:{0}", s, _nLabel));
                }
                if (_args.IsParameterTrue("verbose"))
                    System.Console.WriteLine(s);
            }
            catch (Exception) { }
        }

        static System.Configuration.Install.InstallContext _args = null;
        static string quit_event = "ipswBalance_Quit_Event";
        static void Main(string[] args)
        {
            _args = new System.Configuration.Install.InstallContext(null, args);
            try
            {
                System.Threading.EventWaitHandle quit = null;// new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.AutoReset, quit_event);
                if(_args.Parameters.ContainsKey("label"))
                {
                    if (!Int32.TryParse(_args.Parameters["label"], out _nLabel))
                        _nLabel = 0;
                }
                if (_args.IsParameterTrue("start-server"))
                {
                    try
                    {
                        quit = System.Threading.EventWaitHandle.OpenExisting(quit_event);
                        if (quit != null)
                        {
                            // server already started
                            quit.Close();
                            quit = null;
                        }
                    }
                    catch (System.Threading.WaitHandleCannotBeOpenedException)
                    {
                        quit = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.ManualReset, quit_event);
                    }
                    catch (System.Exception)
                    {
                        quit = null;
                    }

                    if(quit!=null)
                    {
                        //run server
                        DataProvider.StartServer(quit);
                    }

                }
                else if (_args.IsParameterTrue("kill-server"))
                {
                    try
                    {
                        quit = System.Threading.EventWaitHandle.OpenExisting(quit_event);
                        if (quit != null)
                        {
                            quit.Set();
                            quit.Close();
                        }
                    }
                    catch (System.Exception)
                    {

                    }
                }
                else if (_args.Parameters.ContainsKey("path"))
                {
                    String sPath = _args.Parameters["path"];
                    string s = sPath;
                    if (!Properties.Settings.Default.OneDiskUsing)
                    {
                        s = DataProvider.QueryInfo(sPath);
                    }
                    Console.WriteLine(string.Format("@#IPSW#@={0}", s));
                }

            }
            catch (System.Exception ex)
            {
                logIt(string.Format("get special host fail. err:{0}", ex.ToString()));
            }
        }
    }
}
